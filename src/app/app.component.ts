import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Setting} from "../public/setting/setting";

// import {HomePage} from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'HomePage';
  curRoote = location.hash;
  pages: Array<{ title: string, component: any }>;
  public userInfo: any = {};

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      // { title: '首页', component: 'HomePage'},
      {title: '个人中心', component: 'CenterPage'},
      {title: '我的矿机', component: 'MillPage'},
      {title: '邀请码', component: 'InvitePage'}
    ];

    let url = location.href;
    if (url.indexOf('?token=') > 0) {
      let token = url.substring(url.indexOf('=') + 1);
      localStorage.setItem(Setting.storage.authorizationToken, token);
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      let token = localStorage.getItem(Setting.storage.authorizationToken);
      //除了注册页，如果没有token，跳转到登录
      if (!token && this.curRoote.indexOf('register') === -1) {
        this.nav.setRoot('LoginPage');
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }
}
