import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {PublicModule} from "../public/public.module";
import {ServicesModule} from "../services/services.module";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    PublicModule,
    ServicesModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '', // 配置返回按钮的文字,
      platforms: {
        ios: {
          menuType: 'overlay',
        }
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
