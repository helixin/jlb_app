import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'account',
  templateUrl: 'account.html'
})
export class AccountComponent implements OnChanges {
  @Input() readonly: boolean = false;
  @Input() refresh: boolean = false;
  @Input() account: string;//要查询余额的账号
  public accountData: any = {};

  constructor(private userService: UserService) {
  }

  /**
   * 账户余额信息
   */
  getUserBalance() {
    this.userService.getUserBalance().then(data => {
      this.accountData = data
    })
  }

  /**
   * 账户余额信息
   */
  getSubAccount() {
    this.userService.getSubAccount(this.account).then(data => {
      this.accountData = data
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['refresh'] && this.refresh && !this.account) {
      this.getUserBalance();
    }
    if (changes['refresh'] && this.refresh && this.account) {
      this.getSubAccount();
    }
  }

}
