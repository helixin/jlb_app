import { NgModule } from '@angular/core';
import { AccountComponent } from './account/account';
import {IonicModule} from "ionic-angular";
@NgModule({
	declarations: [AccountComponent],
	imports: [IonicModule],
	exports: [AccountComponent]
})
export class ComponentsModule {}
