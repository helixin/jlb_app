import {NgModule} from '@angular/core';
import {Util} from "./service/util";
import {Common} from "./service/common";
import {Ajax} from "./service/ajax";
import {Pattern} from "./service/pattern";
import {RequestFilter} from "./service/request-filter";

@NgModule({
  declarations: [],
  imports: [],
  exports: [],
  providers: [Util, Common, Ajax, Pattern, RequestFilter]
})
export class PublicModule {
}
