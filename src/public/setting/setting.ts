/*基本属性配置*/
export class Setting {
  public static APP: any = {                           //平台信息
    logo: 'assets/imgs/logo.png',
    title: '个人中心'
  };
  //定义枚举
  public static ENUM: any = {
    yesOrNo: 1001,  // 是否
    HttpCodesEnum: {
      Success: '0000', //返回成功
      logout: '9001', //已退出登录
    },
    payTypes: 1006,
    coinTypes: 1003,
    eventTypes: 1004,
    userType: 1018,
  };

  //存入storage信息的键名
  public static storage: any = {
    authorizationToken: 'AuthorizationToken',//存入localStorage的token的键值
  };

  //定义枚举状态名
  public static ENUMSTATE: any = {
    yes: 'Y',
    no: 'N',
    userType: {
      leader: 'Leader',
      member: 'Member',
      areaManager: 'AreaManager',
    },
    minerState: {
      working: 'Working',
      stop: 'Stop'
    },
    oreState: {
      active: 'Active',
      stop: 'Stop'
    },
    digState: {
      digIn: 'Dig_In',
      digOut: 'Dig_Out',
      recorded: 'Recorded'
    },
    coinType: {
      jlb: 'JLB',
      ore: 'ORE'
    },
    payType: {
      jlb: 'Jlb',
      jlbOre: 'Jlb_Ore'
    },
    transferType: {
      in: 'In',
      out: 'Out'
    }
  };
}
