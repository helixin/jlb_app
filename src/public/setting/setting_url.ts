/*接口访问路径配置*/

export class SettingUrl {
  // 接口通讯url集合
  static URL: any = {
    enum: "/res/enum/",
    register: '/reg/user/reg',//注册接口
    login: '/login/user/signin',//登录接口
    logout: "/login/invalidate", //（get）退出登录登录
    getNewList: "/app/news/list",
    loadNews: "/app/news/load",
    appHomeDes: "/app/setting/load",//首页描述
    miner: {
      list: "/app/minerItem/list",//用户查询到的矿机
      purchase: "/app/minerItem/purchase",//购买矿机
      recast: "/app/minerItem/recast",//复投矿机
      upgrade: "/app/minerItem/upgrade",//升级矿机
    },
    user: {
      getUserBalance: "/app/user/getUserBalance",//账户余额信息
      getMyActive: "/app/user/getMyActive",//我的激活币
      getMyDeposit: "/app/user/getMyDeposit",//我的押金
      countMyGroup: "/app/user/countMyGroup",//我的团队人数
      listMyGroup: "/app/user/listMyGroup",//我的团队
      getMyInvite: "/app/user/getMyInvite",//我的直推人数
      getUserMiner: "/app/user/getUserMiner",//我的矿机
      listBindBank: "/app/user/listBindBank",//绑定银行卡列表
      deleteBindBank: "/app/user/deleteBindBank",//删除绑定银行卡
      bindBank: "/app/user/bindBank",//绑定银行卡
      listJlb: "/app/user/listJlb",//金兰宝产生记录
      listOre: "/app/user/listOre",//矿石产生记录
      listActive: "/app/user/listActive",//激活币记录
      listTransferRecord: "/app/user/listTransferRecord",//查询转账列表
      loadInfo: "/app/user/load",//我的个人信息及各种余额信息
      subAccountList: "/app/user/subAccountList",//我的关联子账户
      bindAccount: "/app/user/bindAccount",//关联子账户
      getSubAccount: "/app/user/getSubAccount",//子账户余额
      transferSubAccount: "/app/user/transferSubAccount",//子账户转账
      harvest: "/app/user/harvest",//收取金兰宝
      recast: "/app/user/recast",//用户动态复投
      refineOre: "/app/user/refineOre",//提炼矿石
      transfer: "/app/user/transfer",//用户间转账
      transferActiveCode: "/app/user/transferActiveCode",//用户间互转激活币
      transferJlb: "/app/user/transferJlb",//用户间互转金兰宝
      transferOre: "/app/user/transferOre",//用户间互转转矿石
      updateAlipay: "/app/user/updateAlipay",//updateAlipay
      updateWechat: "/app/user/updateWechat",//updateWechat
      updateIdCard: "/app/user/updateIdCard",//updateIdCard
      updateRealName: "/app/user/updateRealName",//updateRealName
      updateNickName: "/app/user/updateNickName",//updateNickName
      updatePhone: "/app/user/updatePhone",//updatePhone
      updatePassword: "/app/user/updatePassword",//updatePassword
      updatePinPassword: "/app/user/updatePinPassword",//updatePinPassword
      getUser: '/user/load/account'//获取用户信息
    }
  }
  ;
// 路由链接信息
  static ROUTERLINK: any = {
    center: {
      center: '/center/center',
      account: '../account',
      record: '../record',
      spread: '../spread',
      team: '../team',
      trade: '../trade',
      tradePwd: '../tradePwd',
      setTradePwd: '../setTradePwd',
      upPwd: '../upPwd',
    },
    pages: {
      login: "/page/login", //登录
      enroll: "../enroll", //注册
    }
  }
}
