import {Injectable} from "@angular/core";
import {LoadingController, ToastController} from "ionic-angular";

@Injectable()
export class Common {
  constructor(private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  /**
   * 提示
   * @param info
   * 直接.present()
   */
  toast(info) {
    this.toastCtrl.create({
      message: info,
      duration: 3000
    }).present();
  }

  /**
   * 成功提示
   */
  success(info?: string) {
    if (!info) info = '操作成功';
    this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="text-center">
          <i class="iconfont icon-chenggong font40"></i>
        </div>
        <div>${info}</div>`,
      duration: 2000
    }).present();
  }

  /**f
   * 加载中
   * @param content
   * 返回本身，在适当的时候.present()和dismiss
   */
  loading(content) {
    return this.loadingCtrl.create({
      content: content,
      dismissOnPageChange: true
    });
  }

}
