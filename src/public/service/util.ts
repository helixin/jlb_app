import {SettingUrl} from "../setting/setting_url";
import {Injectable} from "@angular/core";
import {Ajax} from "./ajax";

@Injectable()
export class Util {
  public enumData = {};

  constructor(private ajax: Ajax) {
  }

  /**
   * 根据类型标示获取枚举信息
   * @param code 类型标示（如：1001、1002、1003....）
   * @returns {any}
   */
  public getEnumData(code) {
    let me = this;
    if (!this.enumData.hasOwnProperty(code)) {
      this.ajax.get({
        async: false,
        url: SettingUrl.URL.enum + code,
        success: function (result) {
          if (!result) {
            return '';
          } else {
            me.enumData[code] = result;
          }
        }
      });
    }
    return this.enumData[code];
  };

  /**
   * 根据类型标示获取枚举list信息
   * code 类型标示（如：1001、1002、1003....）
   * @param code
   * @returns {Array<any>}
   */
  public getEnumDataList(code) {
    let list: Array<any> = [];
    let enumInfo = this.getEnumData(code);
    for (let prop in enumInfo) {
      if (enumInfo.hasOwnProperty(prop)) {
        list.push({'key': prop, 'val': enumInfo[prop]});
      }
    }
    return list;
  }

  /**
   * 根据类型标示和key获取信息值
   * @param code （如：1001、1002、1003....）
   * @param key （如：ILLNESSCASE、TYPELESS、NURSING....）
   * @returns {any}
   */
  public getEnumDataValByKey(code, key) {
    let enumData = this.getEnumData(code);
    if (enumData != null && enumData !== '' && enumData !== undefined) {
      if (enumData[key] != null && enumData[key] !== '' && enumData[key] !== undefined) {
        return enumData[key];
      } else {
        return '';
      }
    } else {
      return '';
    }
  };

  /**
   * 获取上传文件的uid
   * @returns {any}
   */
  public uploadUid() {
    let uid;
    this.ajax.get({
      url: SettingUrl.URL.base.uuid,
      async: false,
      success: (res) => {
        if (res.success) uid = res.data;
      }
    });
    return uid;
  }

  /**
   * 12位的区域编码根据code查询级别
   * @param areaCode
   * @returns {number}
   */
  public getLevelByCode(areaCode) {
    let level = 0;
    if (!(areaCode)) {
      return level;
    }
    areaCode = areaCode.toString();
    if (areaCode.length != 12) return level;
    if (areaCode.substr(2, 4) == '0000') level = 1;
    else if (areaCode.substr(4, 2) == '00') level = 2;
    else if (areaCode.substr(6, 6) == '000000') level = 3;
    else level = 4;
    return level;
  }

}
