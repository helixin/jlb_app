import {Injectable} from '@angular/core';
import {Ajax} from "../public/service/ajax";
import {Setting} from "../public/setting/setting";
import {SettingUrl} from "../public/setting/setting_url";
import {Common} from "../public/service/common";

@Injectable()
export class PagesService {

  constructor(private ajaxService: Ajax, private commons: Common) {
  }

  /**
   * 注册
   * @param params
   */
  register(params) {
    let me = this;
    return new Promise(function (resolve,reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.register,
        data: params,
        mask: true,
        auth: false,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            me.commons.success(res.info);
            resolve(res.data);
          }else reject(false)
        }
      });
    })
  }

  /**
   * 登录
   * @param params
   */
  login(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.login,
        data: params,
        auth: false,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            me.commons.success(res.info);
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 退出登录
   * @param params
   */
  logout() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.logout,
        success: (res) => {
          if (res.success) {
            localStorage.clear(); //清空所有storage
            resolve('登陆成功');
          }
        }
      });
    })
  }

}
