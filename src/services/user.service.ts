import {Injectable} from '@angular/core';
import {Ajax} from "../public/service/ajax";
import {Setting} from "../public/setting/setting";
import {SettingUrl} from "../public/setting/setting_url";
import {Common} from "../public/service/common";

@Injectable()
export class UserService {

  constructor(private ajaxService: Ajax, private common: Common) {
  }

  /**
   * 账户余额信息
   */
  getUserBalance() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getUserBalance,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的激活币
   */
  getMyActive() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getMyActive,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的个人信息及各种余额信息
   */
  loadInfo() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.loadInfo,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的押金
   */
  getMyDeposit() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getMyDeposit,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的团队人数
   */
  countMyGroup() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.countMyGroup,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的团队
   */
  listMyGroup(userCode?) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listMyGroup,
        data: {userCode: userCode},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的直推人数
   */
  getMyInvite() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getMyInvite,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * updateAlipay
   */
  updateAlipay(alipay) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updateAlipay,
        data: {alipay: alipay},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updateWechat
   */
  updatePhone(phone) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updatePhone,
        data: {phone: phone},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updateWechat
   */
  updateWechat(wechat) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updateWechat,
        data: {wechat: wechat},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updateIdCard
   */
  updateIdCard(idCard) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updateIdCard,
        data: {idCard: idCard},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updateRealName
   */
  updateRealName(realName) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updateRealName,
        data: {realName: realName},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updateNickName
   */
  updateNickName(nickName) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updateNickName,
        data: {nickName: nickName},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updatePassword
   */
  updatePassword(password) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updatePassword,
        data: {password: password},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * updatePinPassword
   */
  updatePinPassword(pinPassword) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.put({
        url: SettingUrl.URL.user.updatePinPassword,
        data: {pinPassword: pinPassword},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * 我的矿机
   */
  getUserMiner() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getUserMiner,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 绑定银行卡列表
   */
  listBindBank() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listBindBank,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 绑定银行卡
   * @param params{bankName,bankNumber,bankMan,subBank}
   */
  bindBank(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.bindBank,
        data: params,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * 删除绑定银行卡
   * @param bankNumber
   */
  deleteBindBank(bankNumber) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.del({
        url: SettingUrl.URL.user.deleteBindBank,
        data: {bankNumber: bankNumber},
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 金兰宝产生记录
   */
  listJlb(curPage = 1, pageSize: number) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listJlb,
        data: {
          curPage: curPage,
          pageSize: pageSize
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 矿石产生记录
   */
  listOre(curPage = 1, pageSize: number) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listOre,
        data: {
          curPage: curPage,
          pageSize: pageSize
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 激活币产生记录
   */
  listActive(curPage = 1, pageSize: number) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listActive,
        data: {
          curPage: curPage,
          pageSize: pageSize
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 查询转账列表
   */
  listTransferRecord(curPage = 1, pageSize: number, type?) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.listTransferRecord,
        data: {
          curPage: curPage,
          pageSize: pageSize,
          type: type
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 我的关联子账户
   */
  subAccountList() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.subAccountList,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 获取用户信息
   * @param data
   */
  getUser(account) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getUser,
        data: {account: account},
        noErrTip: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 关联子账户
   * @param data
   */
  bindAccount(data) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.bindAccount,
        data: data,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * 子账户
   * @param account 账户
   */
  getSubAccount(account: string) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.user.getSubAccount,
        data: {
          account: account
        },
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 子账户转账
   * @param account 账户
   */
  transferSubAccount(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.transferSubAccount,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * 收取金兰宝
   */
  harvest() {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.harvest,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success(res.info);
          }
        }
      });
    })
  }

  /**
   * 用户动态复投{payType,pinPassword}
   */
  recast(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.recast,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('复投成功')
          } else reject(false)
        }
      });
    })
  }

  /**
   * 提炼矿石{num,pinPassword}
   */
  refineOre(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.refineOre,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('提炼成功')
          } else reject(false)
        }
      });
    })
  }

  /**
   * 用户间互转激活币{account,num,pinPassword}
   */
  transfer(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.transfer,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('转账成功')
          } else {
            reject(false)
          }
        }
      });
    })
  }

  /**
   * 用户间互转激活币{account,num,pinPassword}
   */
  transferActiveCode(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.transferActiveCode,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('转账成功')
          } else {
            reject(false)
          }
        }
      });
    })
  }

  /**
   * 用户间互转金兰宝{account,num,pinPassword}
   */
  transferJlb(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.transferJlb,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('转账成功')
          } else {
            reject(false)
          }
        }
      });
    })
  }

  /**
   * 用户间互转转矿石{account,num,pinPassword}
   */
  transferOre(params) {
    let me = this;
    return new Promise(function (resolve, reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.user.transferOre,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('转账成功')
          } else {
            reject(false)
          }
        }
      });
    })
  }

}
