import {NgModule} from '@angular/core';
import {PublicModule} from "../public/public.module";
import {NewsService} from "./news.service";
import {MinerService} from "./miner.service";
import {UserService} from "./user.service";
import {PagesService} from "./pages.service";

@NgModule({
  declarations: [],
  imports: [
    PublicModule
  ],
  exports: [],
  providers: [
    PagesService,
    NewsService,
    MinerService,
    UserService
  ]
})
export class ServicesModule {
}
