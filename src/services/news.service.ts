import {Injectable} from '@angular/core';
import {Ajax} from "../public/service/ajax";
import {Setting} from "../public/setting/setting";
import {SettingUrl} from "../public/setting/setting_url";

@Injectable()
export class NewsService {

  constructor(private ajaxService: Ajax) {
  }
  /**
   * 新闻列表
   * @param params
   */
  getNewList(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.getNewList,
        data: params,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 新闻内容
   * @param code
   */
  loadNews(code) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.loadNews,
        data: {
          code: code
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  appHomeDes(){
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.appHomeDes,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

}
