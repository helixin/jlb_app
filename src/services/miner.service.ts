import {Injectable} from '@angular/core';
import {Ajax} from "../public/service/ajax";
import {Setting} from "../public/setting/setting";
import {SettingUrl} from "../public/setting/setting_url";
import {Common} from "../public/service/common";

@Injectable()
export class MinerService {

  constructor(private ajaxService: Ajax, private common: Common) {
  }

  /**
   * 用户查询到的矿机
   * @param params
   */
  getMinerList(curPage = 1, pageSize: number) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.miner.list,
        data: {
          curPage: curPage,
          pageSize: pageSize
        },
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
          }
        }
      });
    })
  }

  /**
   * 购买矿机
   * @param params {minerCode,payType,pinPassword}
   */
  purchase(params) {
    let me = this;
    return new Promise(function (resolve,reject) {
      me.ajaxService.post({
        url: SettingUrl.URL.miner.purchase,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            me.common.success('购买成功');
            resolve(true)
          } else {
            reject(false)
          }
        }
      });
    })
  }

  /**
   * 复投矿机
   * @param params {payType,pinPassword}
   */
  recast(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.miner.recast,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('复投成功')
          }
        }
      });
    })
  }

  /**
   * 升级矿机
   * @param params {payType,pinPassword}
   */
  upgrade(params) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.post({
        url: SettingUrl.URL.miner.upgrade,
        data: params,
        mask: true,
        success: (res) => {
          if (res.success && res.code === Setting.ENUM.HttpCodesEnum.Success) {
            resolve(res.data);
            me.common.success('升级成功')
          }
        }
      });
    })
  }

}
