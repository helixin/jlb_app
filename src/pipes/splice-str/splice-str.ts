import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'spliceStr',
})
export class SpliceStrPipe implements PipeTransform {
  /**
   * 截取字符串隐藏部分并用*代替
   * @param str
   * @param args [ ,],两个参数分别表示字符串前面保留位数和后面保留位数
   */
  transform(str: string, args:Array<number>) {
    if (!str) return;
    let stars: string = '',
      starsLength = str.length - args[0] - args[1],
      startPlace = args[0],
      endPlace = str.length - args[1];
    for (let i = 0; i < starsLength; i++) stars += '*';
    let strr = str.substr(0, startPlace) + stars + str.substr(endPlace);
    return strr;
  }
}
