import { NgModule } from '@angular/core';
import { TrustHtmlPipe } from './trust-html/trust-html';
import { EnumNamePipe } from './enum-name/enum-name';
import { SpliceStrPipe } from './splice-str/splice-str';
@NgModule({
	declarations: [TrustHtmlPipe,
    EnumNamePipe,
    SpliceStrPipe],
	imports: [],
	exports: [TrustHtmlPipe,
    EnumNamePipe,
    SpliceStrPipe]
})
export class PipesModule {}
