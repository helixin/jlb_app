import { Pipe, PipeTransform } from '@angular/core';
import {Util} from "../../public/service/util";

@Pipe({
  name: 'enumName',
})
export class EnumNamePipe implements PipeTransform {
  constructor(private util: Util) {
  }

  transform(value: any, args?: any): any {
    return this.util.getEnumDataValByKey(args, value);
  }
}
