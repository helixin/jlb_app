import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpPasswordPage } from './up-password';

@NgModule({
  declarations: [
    UpPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(UpPasswordPage),
  ],
})
export class UpPasswordPageModule {}
