import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Setting} from "../../public/setting/setting";
import {UserService} from "../../services/user.service";
import {MinerService} from "../../services/miner.service";
import {Page} from "../../public/page";

@IonicPage()
@Component({
  selector: 'page-sub-account-transfer',
  templateUrl: 'sub-account-transfer.html',
})
export class SubAccountTransferPage {
  public subAccount;
  public validateForm: FormGroup = this.fb.group({
    account: [null, [Validators.required]],
    jlb: [null],
    ore: [null],
    active: [null],
    coin: [null],
    num: [null],
    miner: [null],
    buyType: [null],
    quickTransfer: [true],
  });//登录的表单
  public isRefresh: boolean = true;
  public state = Setting.ENUMSTATE;
  minerList: Array<any> = [];
  buyTypes: Array<any> = ["金兰宝", "金兰宝+矿石"];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private fb: FormBuilder, private alertCtrl: AlertController,
              private view: ViewController,
              private userService: UserService, private minerService: MinerService) {
    this.subAccount = this.navParams.get('subAccount');
  }

  ionViewDidLoad() {
    this.getMinerList();
    this.validateForm = this.fb.group({
      account: [null, [Validators.required]],
      jlb: [null, [Validators.required]],
      ore: [null, [Validators.required]],
      active: [null, [Validators.required]],
      coin: [null],
      num: [null],
      miner: [null, [Validators.required]],
      buyType: [null, [Validators.required]],
      quickTransfer: [true],
    })
  }

  changeTransfer() {
    if (this.validateForm.value.quickTransfer) {
      this.validateForm = this.fb.group({
        account: [null, [Validators.required]],
        jlb: [null, [Validators.required]],
        ore: [null, [Validators.required]],
        active: [null, [Validators.required]],
        coin: [null],
        num: [null],
        miner: [null, [Validators.required]],
        buyType: [null, [Validators.required]],
        quickTransfer: [true],
      })
    } else {
      this.validateForm = this.fb.group({
        account: [null, [Validators.required]],
        jlb: [null],
        ore: [null],
        active: [null],
        coin: [null, [Validators.required]],
        num: [null, [Validators.required]],
        miner: [null],
        buyType: [null],
        quickTransfer: [false],
      })
    }
  }

  getBuyType() {
    this.getCoinsNum()
  }

  getCoinsNum() {
    let miner = this.validateForm.value.miner;
    if (!miner) return;
    let transferData: any = {};
    if (miner.isActiveCoin === this.state.yes) {
      this.buyTypes = ["金兰宝+激活币", "金兰宝+矿石+激活币"];
      if (this.validateForm.value.buyType === 1) {
        transferData = {
          jlb: miner.jlb,
          ore: 0,
          active: miner.active
        };
      } else {
        transferData = {
          jlb: miner.mixJlb,
          ore: miner.mixOre,
          active: miner.active
        };
      }
    } else {
      this.buyTypes = ["金兰宝", "金兰宝+矿石"];
      if (this.validateForm.value.buyType === 1) {
        transferData = {
          jlb: miner.jlb,
          ore: 0,
          active: 0
        };
      } else {
        transferData = {
          jlb: miner.mixJlb,
          ore: miner.mixOre,
          active: 0
        };
      }
    }
    console.log("█ transferData ►►►", transferData);
    this.validateForm.patchValue(transferData)
  }

  getMinerList() {
    this.minerService.getMinerList(1, 20).then((data: Page) => {
      this.minerList = data.voList;
    })
  }

  public transferForm($event) {
    $event.preventDefault();
    let me = this;
    for (const key in me.validateForm.controls) {
      me.validateForm.controls[key].markAsDirty();
      me.validateForm.controls[key].updateValueAndValidity();
    }
    if (me.validateForm.invalid) return;
    this.passwordPrompt();
  };

  getFormControl(name) {
    return this.validateForm.controls[name];
  }

  passwordPrompt() {
    this.isRefresh = false;
    let alert = this.alertCtrl.create({
      title: '交易密码',
      inputs: [
        {
          name: 'password',
          type: 'password',
          placeholder: '请输入交易密码'
        }
      ],
      buttons: [
        {
          text: '确认',
          handler: data => {
            if (!data.password || data.password === '') return false;
            else {
              let formData: any;
              if (this.validateForm.value.quickTransfer) {
                formData = {
                  account: this.validateForm.value.account,
                  jlb: this.validateForm.value.jlb,
                  ore: this.validateForm.value.ore,
                  active: this.validateForm.value.active,
                  pinPassword: data.password,
                };
              } else {
                formData = {
                  account: this.validateForm.value.account,
                  pinPassword: data.password,
                };
                switch (this.validateForm.value.coin) {
                  case 'jlb':
                    formData.jlb = this.validateForm.value.num;
                    break;
                  case 'ore':
                    formData.ore = this.validateForm.value.num;
                    break;
                  case 'active':
                    formData.active = this.validateForm.value.num;
                    break;
                }
              }
              formData.subAccount = this.subAccount;
              this.transferSubAccount(formData);
            }
          }
        }
      ]
    });
    alert.present();
  }

  /**
   * 用户间转账
   * @param formData
   */
  transferSubAccount(formData) {
    this.userService.transferSubAccount(formData).then(res => {
      this.isRefresh = true;
      this.validateForm.reset();
    }).catch(err => {
      return false;
    });
  }

  dismiss() {
    this.view.dismiss();
  }

}
