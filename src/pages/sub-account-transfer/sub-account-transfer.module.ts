import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubAccountTransferPage } from './sub-account-transfer';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    SubAccountTransferPage,
  ],
  imports: [
    IonicPageModule.forChild(SubAccountTransferPage),
    ComponentsModule,
  ],
})
export class SubAccountTransferPageModule {}
