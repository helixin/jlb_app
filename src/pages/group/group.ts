import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {Page} from "../../public/page";
import * as $ from 'jquery';

@IonicPage()
@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService) {
  }

  ionViewDidLoad() {
    this.listMyGroup();
    let me = this;
    $(function () {
      $('#nodes').on('click', '.more', function () {
        if ($(this).children('.group-item').length > 0) {
          $(this).children('.group-item').eq(0).show()
        } else {
          me.listMyGroup($(this).attr('data'), this);
        }
      });
      $('#nodes').on('click', '.less', function () {
        $(this).parents('.group-item').eq(0).children('.group-item').hide();
        $(this).html('+').removeClass('less').addClass('more')
      })
    })
  }

  listMyGroup(userCode?, target?) {
    this.userService.listMyGroup(userCode).then((res: Page) => {
      let parentNode;
      if (!target) parentNode = $('#nodes');
      else parentNode = $(target).parents('.group-item').eq(0);
      let notes = '';
      res.voList.forEach(item => {
        let str = ``;
        if (item.haveChild === 'Y') {
          str = `<div class="inline-block color-blue more" data="${item.userCode}">+</div>`;
        }
        notes += `<div class="group-item ${userCode ? 'ml-20' : ''}">` + str
          + `<div class="${item.haveChild === 'Y' ? 'inline-block' : 'no-child'}">
        ${item.account} <span class="color-orange ml-5">[${item.realName}]</span>
          </div>
        </div>`
      });
      $(target).html('-').removeClass('more').addClass('less');
      parentNode.append(notes);
    })
  }

}
