import {Component} from '@angular/core';
import {InfiniteScroll, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {Page} from "../../public/page";
import {Setting} from "../../public/setting/setting";

@IonicPage()
@Component({
  selector: 'page-jlb',
  templateUrl: 'jlb.html',
})
export class JlbPage {
  jlbRecord: Page = new Page();
  scrollEnable:boolean = true;
  public digState = Setting.ENUMSTATE.digState;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService) {

  }

  ionViewDidLoad() {
    this.doInfinite();
  }

  doInfinite(infiniteScroll?: InfiniteScroll) {
    this.userService.listJlb(this.jlbRecord.curPage, this.jlbRecord.pageSize)
      .then((data: Page) => {
        this.jlbRecord.curPage += 1;//请求成功加一页
        this.jlbRecord.voList = this.jlbRecord.voList.concat(data.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.voList.length < this.jlbRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.scrollEnable = false;
        }
      }).catch(res => {
      if (infiniteScroll) infiniteScroll.complete();
    })
  }
}
