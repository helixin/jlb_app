import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JlbPage } from './jlb';

@NgModule({
  declarations: [
    JlbPage,
  ],
  imports: [
    IonicPageModule.forChild(JlbPage),
  ],
})
export class JlbPageModule {}
