import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyMillPage } from './buy-mill';

@NgModule({
  declarations: [
    BuyMillPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyMillPage),
  ],
})
export class BuyMillPageModule {}
