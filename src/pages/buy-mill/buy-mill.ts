import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MinerService} from "../../services/miner.service";
import {Util} from "../../public/service/util";
import {Setting} from "../../public/setting/setting";

@IonicPage()
@Component({
  selector: 'page-buy-mill',
  templateUrl: 'buy-mill.html',
})
export class BuyMillPage {
  minerCode: string;//购买的矿机编码
  public validateForm: FormGroup = this.fb.group({
    payType: [null, [Validators.required]],
  });//表单
  payTypes: Array<{ key, val }> = [];

  constructor(private fb: FormBuilder, public navCtrl: NavController, private util: Util,
              private alertCtrl: AlertController, private minerService: MinerService,
              public navParams: NavParams, private view: ViewController) {
    this.minerCode = this.navParams.get('code');
  }

  ionViewDidLoad() {
    this.payTypes = this.util.getEnumDataList(Setting.ENUM.payTypes);
  }

  buyMill($event) {
    $event.preventDefault();
    let me = this;
    for (const key in me.validateForm.controls) {
      me.validateForm.controls[key].markAsDirty();
      me.validateForm.controls[key].updateValueAndValidity();
    }
    if (me.validateForm.invalid) return;
    this.passwordPrompt();
  };

  passwordPrompt() {
    let alert = this.alertCtrl.create({
      title: '交易密码',
      inputs: [
        {
          name: 'password',
          type: 'password',
          placeholder: '请输入交易密码'
        }
      ],
      buttons: [
        {
          text: '确认',
          handler: data => {
            if (!data.password || data.password === '') return false;
            else {
              this.minerService.purchase({
                minerCode: this.minerCode,
                payType: this.validateForm.value.payType,
                pinPassword: data.password
              }).then(res => {
                this.dismiss();
              }).catch(err => {
                return false
              });
            }
          }
        }
      ]
    });
    alert.present();
  }

  getFormControl(name) {
    return this.validateForm.controls[name];
  }

  dismiss() {
    this.view.dismiss();
  }

}
