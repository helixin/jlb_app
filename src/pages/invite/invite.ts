import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import QRCode from 'qrcode'
import {UserService} from "../../services/user.service";
import {Setting} from "../../public/setting/setting";

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {
  public userInfo: any = {};
  public inviteCodeImgUrl: string = Setting.APP.logo;
  private url = location.origin;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService) {
  }

  ionViewDidLoad() {
    this.userService.loadInfo().then((data: any) => {
      this.userInfo = data;
      QRCode.toDataURL(this.url + '/#/register?account=' + data.account, {
        width: 300,
        height: 300
      }).then(url => {
        this.inviteCodeImgUrl = url;
      })
    })
  }
}
