import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CenterPage } from './center';
import {ComponentsModule} from "../../components/components.module";
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    CenterPage,
  ],
  imports: [
    IonicPageModule.forChild(CenterPage),
    ComponentsModule,
    PipesModule,
  ],
})
export class CenterPageModule {}
