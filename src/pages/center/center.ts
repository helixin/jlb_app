import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {Setting} from "../../public/setting/setting";
import {Common} from "../../public/service/common";

@IonicPage()
@Component({
  selector: 'page-center',
  templateUrl: 'center.html',
})
export class CenterPage {
  public userInfo: any = {};
  public myData: any = {};
  public accountData: any = {};
  public isRefreshAccount: boolean = false;//转账回来需要刷新账户余额，用此字段标记是否需要刷新
  public userType = Setting.ENUM.userType;
  public userTypes = Setting.ENUMSTATE.userType;
  public quickTransfer: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService, private common: Common) {

  }

  ionViewDidEnter() {
    this.isRefreshAccount = true;
    this.loadInfo();//我的个人信息及各种余额信息
  }

  ionViewDidLeave() {
    this.isRefreshAccount = false;
  }

  goHome() {
    this.navCtrl.setRoot('HomePage');
  }

  isReactive() {
    if (this.userInfo.userOre && this.userInfo.userOre.state === Setting.ENUMSTATE.oreState.stop) {
      this.navCtrl.setRoot('ReactivePage');
    } else {
      this.common.toast('您的动态收益未达上限，不需要动态复投')
    }
  }

  /**
   * 账户余额信息
   */
  getUserBalance() {
    this.userService.getUserBalance().then(data => {
      this.accountData = data
    })
  }

  /**
   * 我的个人信息及各种余额信息
   */
  loadInfo() {
    this.userService.loadInfo().then(data => {
      this.userInfo = data;
      this.getUserBalance();
      this.getMyDeposit();//我的押金
    })
  }

  /**
   * 我的押金
   */
  getMyDeposit() {
    this.userService.getMyDeposit().then(data => {
      this.myData.deposit = data;
    })
  }

}
