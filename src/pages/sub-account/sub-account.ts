import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";

@IonicPage()
@Component({
  selector: 'page-sub-account',
  templateUrl: 'sub-account.html',
})
export class SubAccountPage {
  public subAccounts: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService,
              public modalCtrl: ModalController) {
  }


  ionViewDidEnter() {
    this.subAccountList()
  }

  subAccountList() {
    this.userService.subAccountList().then((data: any) => {
      this.subAccounts = data;
    })
  }

  transferPage(subAccount) {
    const modal = this.modalCtrl.create('SubAccountTransferPage', {subAccount: subAccount});
    modal.present();
  }

}
