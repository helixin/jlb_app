import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReactivePage } from './reactive';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    ReactivePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ReactivePage),
  ],
})
export class ReactivePageModule {}
