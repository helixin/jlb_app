import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExtractPage } from './extract';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    ExtractPage,
  ],
  imports: [
    IonicPageModule.forChild(ExtractPage),
    ComponentsModule
  ],
})
export class ExtractPageModule {}
