import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {NewsService} from "../../services/news.service";

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  private newsId;
  public news: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private newsService: NewsService, private view: ViewController) {
    this.newsId = this.navParams.get('code');
  }

  ionViewDidLoad() {
    this.getNewsByCode();
  }

  getNewsByCode() {
    this.newsService.loadNews(this.newsId).then((res: any) => {
      this.news = res;
    })
  }

  dismiss() {
    this.view.dismiss();
  }

}
