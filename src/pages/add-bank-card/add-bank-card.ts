import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Pattern} from "../../public/service/pattern";


@IonicPage()
@Component({
  selector: 'page-add-bank-card',
  templateUrl: 'add-bank-card.html',
})
export class AddBankCardPage {
  public validateForm: FormGroup = this.fb.group({
    bankName: [null, [Validators.required]],
    bankMan: [null, [Validators.required]],
    subBank: [null, [Validators.required]],
    bankNumber: [null, Validators.compose([Validators.required,Validators.pattern(Pattern.bankcard)])],
  });//登录的表单

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private fb: FormBuilder,private userService: UserService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddBankCardPage');
  }

  bindCard($event){
    $event.preventDefault();
    let me = this;
    for (const key in me.validateForm.controls) {
      me.validateForm.controls[key].markAsDirty();
      me.validateForm.controls[key].updateValueAndValidity();
    }
    if (me.validateForm.invalid) return;
    me.userService.bindBank(me.validateForm.value).then(res => {
      this.navCtrl.pop()
    })
  }


  getFormControl(name) {
    return this.validateForm.controls[name];
  }

}
