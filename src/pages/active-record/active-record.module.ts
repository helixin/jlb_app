import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveRecordPage } from './active-record';

@NgModule({
  declarations: [
    ActiveRecordPage,
  ],
  imports: [
    IonicPageModule.forChild(ActiveRecordPage),
  ],
})
export class ActiveRecordPageModule {}
