import {Component} from '@angular/core';
import {InfiniteScroll, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Page} from "../../public/page";
import {UserService} from "../../services/user.service";

@IonicPage()
@Component({
  selector: 'page-active-record',
  templateUrl: 'active-record.html',
})
export class ActiveRecordPage {
  activeRecord: Page = new Page();
  allEnable: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService) {
  }

  ionViewDidLoad() {
    this.doInfinite();
  }

  doInfinite(infiniteScroll?: InfiniteScroll) {
    this.userService.listActive(this.activeRecord.curPage, this.activeRecord.pageSize)
      .then((data: any) => {
        this.activeRecord.curPage += 1;//请求成功加一页
        this.activeRecord.voList = this.activeRecord.voList.concat(data.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.voList.length < this.activeRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.allEnable = false;
        }
      }).catch(err => {
      if (infiniteScroll) infiniteScroll.complete();
    });
  }

}
