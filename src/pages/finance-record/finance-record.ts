import {Component} from '@angular/core';
import {InfiniteScroll, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";
import {Page} from "../../public/page";
import {Setting} from "../../public/setting/setting";

/**
 * Generated class for the FinanceRecordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finance-record',
  templateUrl: 'finance-record.html',
})
export class FinanceRecordPage {
  public enums = Setting.ENUM;
  public transferTypes = Setting.ENUMSTATE.transferType;
  public record: string = 'all';
  public userCode: string;
  allRecord: Page = new Page();
  inRecord: Page = new Page();
  outRecord: Page = new Page();
  allEnable: boolean = true;
  inEnable: boolean = true;
  outEnable: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService) {
  }

  ionViewDidLoad() {
    this.loadMoreAllRecord();
  }

  curChecked(e) {
    switch (e) {
      case 'all': {
        if (this.allRecord.curPage === 1) this.loadMoreAllRecord();
        break;
      }
      case 'in': {
        if (this.inRecord.curPage === 1) this.loadMoreInRecord();
        break;
      }
      case 'out': {
        if (this.outRecord.curPage === 1) this.loadMoreOutRecord();
        break;
      }
    }
  }

  loadMoreAllRecord(infiniteScroll?: InfiniteScroll) {
    this.userService.listTransferRecord(this.allRecord.curPage, this.allRecord.pageSize)
      .then((data: any) => {
        this.userCode = data.userCode;
        this.allRecord.curPage += 1;//请求成功加一页
        this.allRecord.voList = this.allRecord.voList.concat(data.page.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.page.voList.length < this.allRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.allEnable = false;
        }
      }).catch(err => {
      if (infiniteScroll) infiniteScroll.complete();
    });
  }

  loadMoreInRecord(infiniteScroll?: InfiniteScroll) {
    this.userService.listTransferRecord(this.inRecord.curPage, this.inRecord.pageSize, this.transferTypes.in)
      .then((data: any) => {
        this.inRecord.curPage += 1;//请求成功加一页
        this.inRecord.voList = this.inRecord.voList.concat(data.page.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.page.voList.length < this.inRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.inEnable = false;
        }
      }).catch(err => {
      if (infiniteScroll) infiniteScroll.complete();
    });
  }


  loadMoreOutRecord(infiniteScroll?: InfiniteScroll) {
    this.userService.listTransferRecord(this.outRecord.curPage, this.outRecord.pageSize, this.transferTypes.out)
      .then((data: any) => {
        this.outRecord.curPage += 1;//请求成功加一页
        this.outRecord.voList = this.outRecord.voList.concat(data.page.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.page.voList.length < this.outRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.outEnable = false;
        }
      }).catch(err => {
      if (infiniteScroll) infiniteScroll.complete();
    });
  }

}
