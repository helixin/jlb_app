import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinanceRecordPage } from './finance-record';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    FinanceRecordPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(FinanceRecordPage),
  ],
})
export class FinanceRecordPageModule {}
