import {Component} from '@angular/core';
import {InfiniteScroll, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Page} from "../../public/page";
import {UserService} from "../../services/user.service";

@IonicPage()
@Component({
  selector: 'page-ore',
  templateUrl: 'ore.html',
})
export class OrePage {
  oreRecord: Page = new Page();
  scrollEnable: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService) {

  }

  ionViewDidLoad() {
    this.doInfinite();
  }

  doInfinite(infiniteScroll?: InfiniteScroll) {
    this.userService.listOre(this.oreRecord.curPage, this.oreRecord.pageSize)
      .then((data: Page) => {
        this.oreRecord.curPage += 1;//请求成功加一页
        this.oreRecord.voList = this.oreRecord.voList.concat(data.voList);
        if (infiniteScroll) infiniteScroll.complete();
        if (data.voList.length < this.oreRecord.pageSize) {
          if (infiniteScroll) infiniteScroll.enable(false);
          this.scrollEnable = false;
        }
      }).catch(res => {
      if (infiniteScroll) infiniteScroll.complete();
    })
  }
}
