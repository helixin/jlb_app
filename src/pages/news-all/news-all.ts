import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Page} from "../../public/page";
import {NewsService} from "../../services/news.service";

@IonicPage()
@Component({
  selector: 'page-news-all',
  templateUrl: 'news-all.html',
})
export class NewsAllPage {
  public newList: Page = new Page();

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private newsService: NewsService,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    this.getNewList();
  }

  /**
   * 获取新闻列表
   */
  getNewList() {
    let params = {
      curPage: this.newList.curPage,
      pageSize: this.newList.pageSize
    };
    this.newsService.getNewList(params).then((res: Page) => {
      this.newList = res;
    })
  }

  getNews(code) {
    const modal = this.modalCtrl.create('NewsPage', {code: code});
    modal.present();
  }

  doInfinite(infiniteScroll) {
    let params = {
      curPage: this.newList.curPage + 1,
      pageSize: this.newList.pageSize
    };
    this.newsService.getNewList(params).then((res: Page) => {
      this.newList.voList = this.newList.voList.concat(res.voList);
      if (res.voList.length < this.newList.pageSize) infiniteScroll.enable(false);
      infiniteScroll.complete();
    }).catch(res => {
      infiniteScroll.complete();
    })
  }
}
