import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsAllPage } from './news-all';

@NgModule({
  declarations: [
    NewsAllPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsAllPage),
  ],
})
export class NewsAllPageModule {}
