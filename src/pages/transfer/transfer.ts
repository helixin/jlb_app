import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Page} from "../../public/page";
import {MinerService} from "../../services/miner.service";
import {Setting} from "../../public/setting/setting";

@IonicPage()
@Component({
  selector: 'page-transfer',
  templateUrl: 'transfer.html',
})
export class TransferPage {
  public validateForm: FormGroup = this.fb.group({
    account: [null, [Validators.required]],
    jlb: [null],
    ore: [null],
    active: [null],
    coin: [null],
    num: [null],
    miner: [null],
    buyType: [null],
    quickTransfer: [true],
  });//登录的表单
  public isRefresh: boolean = true;
  public state = Setting.ENUMSTATE;
  public realName: string;
  minerList: Array<any> = [];
  buyTypes: Array<any> = ["金兰宝", "金兰宝+矿石"];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private fb: FormBuilder, private alertCtrl: AlertController,
              private userService: UserService, private minerService: MinerService) {
  }

  ionViewDidLoad() {
    this.getMinerList();
    this.validateForm = this.fb.group({
      account: [null, [Validators.required]],
      jlb: [null, [Validators.required]],
      ore: [null, [Validators.required]],
      active: [null, [Validators.required]],
      coin: [null],
      num: [null],
      miner: [null, [Validators.required]],
      buyType: [null, [Validators.required]],
      quickTransfer: [true],
    })
  }

  getUser() {
    if (!this.validateForm.value.account || this.validateForm.value.account == '') {
      this.realName = undefined;
      return;
    }
    this.userService.getUser(this.validateForm.value.account).then((data: any) => {
      if (data) this.realName = data.realName;
      else this.realName = '?';
    })
  }

  changeTransfer() {
    if (this.validateForm.value.quickTransfer) {
      this.validateForm = this.fb.group({
        account: [null, [Validators.required]],
        jlb: [null, [Validators.required]],
        ore: [null, [Validators.required]],
        active: [null, [Validators.required]],
        coin: [null],
        num: [null],
        miner: [null, [Validators.required]],
        buyType: [null, [Validators.required]],
        quickTransfer: [true],
      })
    } else {
      this.validateForm = this.fb.group({
        account: [null, [Validators.required]],
        jlb: [null],
        ore: [null],
        active: [null],
        coin: [null, [Validators.required]],
        num: [null, [Validators.required]],
        miner: [null],
        buyType: [null],
        quickTransfer: [false],
      })
    }
  }

  getBuyType() {
    this.getCoinsNum()
  }

  getCoinsNum() {
    let miner = this.validateForm.value.miner;
    if (!miner) return;
    let transferData: any = {};
    if (miner.isActiveCoin === this.state.yes) {
      this.buyTypes = ["金兰宝+激活币", "金兰宝+矿石+激活币"];
      if (this.validateForm.value.buyType === 1) {
        transferData = {
          jlb: miner.jlb,
          ore: 0,
          active: miner.active
        };
      } else {
        transferData = {
          jlb: miner.mixJlb,
          ore: miner.mixOre,
          active: miner.active
        };
      }
    } else {
      this.buyTypes = ["金兰宝", "金兰宝+矿石"];
      if (this.validateForm.value.buyType === 1) {
        transferData = {
          jlb: miner.jlb,
          ore: 0,
          active: 0
        };
      } else {
        transferData = {
          jlb: miner.mixJlb,
          ore: miner.mixOre,
          active: 0
        };
      }
    }
    this.validateForm.patchValue(transferData)
  }

  getMinerList() {
    this.minerService.getMinerList(1, 20).then((data: Page) => {
      this.minerList = data.voList;
    })
  }

  public transferForm($event) {
    $event.preventDefault();
    let me = this;
    for (const key in me.validateForm.controls) {
      me.validateForm.controls[key].markAsDirty();
      me.validateForm.controls[key].updateValueAndValidity();
    }
    if (me.validateForm.invalid) return;
    this.passwordPrompt();
  };

  getFormControl(name) {
    return this.validateForm.controls[name];
  }

  passwordPrompt() {
    let alert = this.alertCtrl.create({
      title: '交易密码',
      inputs: [
        {
          name: 'password',
          type: 'password',
          placeholder: '请输入交易密码'
        }
      ],
      buttons: [
        {
          text: '确认',
          handler: data => {
            if (!data.password || data.password === '') return false;
            else {
              if (this.validateForm.value.quickTransfer) {
                let formData = {
                  account: this.validateForm.value.account,
                  jlb: this.validateForm.value.jlb,
                  ore: this.validateForm.value.ore,
                  active: this.validateForm.value.active,
                  pinPassword: data.password,
                };
                this.transfer(formData);
              } else {
                let formData = {
                  account: this.validateForm.value.account,
                  num: this.validateForm.value.num,
                  pinPassword: data.password,
                };
                switch (this.validateForm.value.coin) {
                  case 'jlb':
                    this.transferJlb(formData);
                    break;
                  case 'ore':
                    this.transferOre(formData);
                    break;
                  case 'active':
                    this.transferActiveCode(formData);
                    break;
                }
              }
              this.isRefresh = false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  /**
   * 用户间转账
   * @param formData
   */
  transfer(formData) {
    this.userService.transfer(formData).then(res => {
      this.isRefresh = true;
      this.validateForm.reset();
    }).catch(err => {
      return false;
    });
  }

  /**
   * 用户间互转激活币
   * @param formData
   */
  transferActiveCode(formData) {
    this.userService.transferActiveCode(formData).then(res => {
      this.isRefresh = true;
      this.validateForm.reset();
    }).catch(err => {
      return false;
    });
  }

  /**
   * 用户间互转金兰宝
   * @param formData
   */
  transferJlb(formData) {
    this.userService.transferJlb(formData).then(res => {
      this.isRefresh = true;
      this.validateForm.reset();
    }).catch(err => {
      return false;
    })
  }

  /**
   * 用户间互转转矿石
   * @param formData
   */
  transferOre(formData) {
    this.userService.transferOre(formData).then(res => {
      this.isRefresh = true;
    }).catch(err => {
      return false;
    })
  }

}
