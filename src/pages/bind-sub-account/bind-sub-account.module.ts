import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BindSubAccountPage } from './bind-sub-account';

@NgModule({
  declarations: [
    BindSubAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(BindSubAccountPage),
  ],
})
export class BindSubAccountPageModule {}
