import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {MinerService} from "../../services/miner.service";
import {Page} from "../../public/page";

@IonicPage()
@Component({
  selector: 'page-mills',
  templateUrl: 'mills.html',
})
export class MillsPage {
  minerList: Page = new Page();

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public modalCtrl: ModalController, private minerService: MinerService) {
  }

  ionViewDidEnter() {
    this.getMinerList();
  }

  getMinerList() {
    this.minerService.getMinerList(1, 20).then((data: Page) => {
      this.minerList = data;
    })
  }

  buyMill(code) {
    const modal = this.modalCtrl.create('BuyMillPage', {code: code});
    modal.present();
  }
}
