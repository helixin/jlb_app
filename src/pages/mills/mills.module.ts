import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MillsPage } from './mills';

@NgModule({
  declarations: [
    MillsPage,
  ],
  imports: [
    IonicPageModule.forChild(MillsPage),
  ],
})
export class MillsPageModule {}
