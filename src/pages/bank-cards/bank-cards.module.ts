import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankCardsPage } from './bank-cards';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    BankCardsPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(BankCardsPage),
  ],
})
export class BankCardsPageModule {}
