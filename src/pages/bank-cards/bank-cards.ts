import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {UserService} from "../../services/user.service";

@IonicPage()
@Component({
  selector: 'page-bank-cards',
  templateUrl: 'bank-cards.html',
})
export class BankCardsPage {
  bankCards: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private userService: UserService,public alertCtrl: AlertController) {
  }

  ionViewDidEnter() {
    this.listBindBank()
  }

  listBindBank() {
    this.userService.listBindBank().then((data: any) => {
      this.bankCards = data;
    })
  }

  deleteBindBank(bankNumber){
    const confirm = this.alertCtrl.create({
      title: '操作确定',
      message: '确定取消绑定这个银行卡吗？',
      buttons: [
        {
          text: '取消',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '确定',
          handler: () => {
            this.userService.deleteBindBank(bankNumber).then(res => {
              this.listBindBank();
            })
          }
        }
      ]
    });
    confirm.present();
  }

}
