import {Component, ViewChild} from '@angular/core';
import {IonicPage, ModalController, NavController, Slides} from 'ionic-angular';
import {Setting} from "../../public/setting/setting";
import {Page} from "../../public/page";
import {NewsService} from "../../services/news.service";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public pics: Array<any> = [];
  public newList: Page = new Page();
  public description: any;
  @ViewChild('slides') slides: Slides;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private newsService: NewsService) {
    let token = localStorage.getItem(Setting.storage.authorizationToken);
    if (!token) this.navCtrl.setRoot('LoginPage');//如果没有token，跳转到登录
  }

  ionViewDidLoad() {
    this.loadDes();
    this.getNewList();
    this.pics = [
      'assets/imgs/slide1.jpeg',
      'assets/imgs/slide1.jpeg',
      'assets/imgs/slide1.jpeg',
      'assets/imgs/slide1.jpeg'
    ]
  }

  getNews(code) {
    const modal = this.modalCtrl.create('NewsPage', {code: code});
    modal.present();
  }

  /**
   * 获取新闻列表
   */
  getNewList() {
    let params = {
      curPage: 1,
      pageSize: 3
    };
    this.newsService.getNewList(params).then((res: Page) => {
      this.newList = res;
    })
  }

  loadDes() {
    this.newsService.appHomeDes().then((data: any) => {
      this.description = data.description;
    })
  }

}
