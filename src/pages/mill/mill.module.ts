import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MillPage } from './mill';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    MillPage,
  ],
  imports: [
    IonicPageModule.forChild(MillPage),
    ComponentsModule
  ],
})
export class MillPageModule {}
